<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/machtig/templates/system/page--machtig-page.html.twig */
class __TwigTemplate_af96c6713af4862e78e1fefc06fff0958e80c665652f31266a392991689c9046 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "
";
        // line 55
        $context["container"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["theme"] ?? null), "settings", [], "any", false, false, true, 55), "fluid_container", [], "any", false, false, true, 55)) ? ("container d-flex align-items-center justify-content-between") : ("container d-flex align-items-center justify-content-between"));
        // line 57
        if ((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation", [], "any", false, false, true, 57) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 57))) {
            // line 58
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 79
        echo "
";
        // line 81
        $this->displayBlock('main', $context, $blocks);
        // line 149
        echo "

<a href=\"#\" class=\"back-to-top d-flex align-items-center justify-content-center\"><i class=\"bi bi-arrow-up-short\"></i></a>


<footer id=\"footer\">
  <div class=\"footer-top\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-contact\" data-aos=\"fade-down\">
        ";
        // line 159
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_first", [], "any", false, false, true, 159)) {
            // line 160
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_first", [], "any", false, false, true, 160), 160, $this->source), "html", null, true);
            echo "
          ";
        }
        // line 162
        echo "        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Useful Links</h4>
          ";
        // line 165
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_second", [], "any", false, false, true, 165)) {
            // line 166
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_second", [], "any", false, false, true, 166), 166, $this->source), "html", null, true);
            echo "
          ";
        }
        // line 168
        echo "        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Our Services</h4>
          ";
        // line 171
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_third", [], "any", false, false, true, 171)) {
            // line 172
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_third", [], "any", false, false, true, 172), 172, $this->source), "html", null, true);
            echo "
          ";
        }
        // line 174
        echo "        </div>
      </div>
    </div>
  </div>

  <div class=\"container d-md-flex py-4\">
    ";
        // line 180
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 180)) {
            // line 181
            echo "      ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 181), 181, $this->source), "html", null, true);
            echo "
    ";
        }
        // line 183
        echo "    </div>
</footer>";
    }

    // line 58
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    ";
        // line 60
        $context["navbar_classes"] = [0 => "fixed-top"];
        // line 64
        echo "    <header";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "addClass", [0 => ($context["navbar_classes"] ?? null)], "method", false, false, true, 64), 64, $this->source), "html", null, true);
        echo " id=\"header\" role=\"banner\">
      ";
        // line 65
        if ( !twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method", false, false, true, 65)) {
            // line 66
            echo "        <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null), 66, $this->source), "html", null, true);
            echo "\">
      ";
        }
        // line 68
        echo "      <h1 class=\"logo\"><img src=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logopath"] ?? null), 68, $this->source), "html", null, true);
        echo "\"></h1>
      ";
        // line 69
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 69)) {
            // line 70
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 70), 70, $this->source), "html", null, true);
            echo "
      ";
        }
        // line 72
        echo "
      ";
        // line 73
        if ( !twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method", false, false, true, 73)) {
            // line 74
            echo "        </div>
      ";
        }
        // line 76
        echo "    </header>
  ";
    }

    // line 81
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 82
        echo "  <main id=\"main\">
      ";
        // line 84
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 84)) {
            // line 85
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 90
            echo "      ";
        }
        // line 91
        echo "
      ";
        // line 93
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 93)) {
            // line 94
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 99
            echo "      ";
        }
        // line 100
        echo "
      ";
        // line 102
        echo "      ";
        // line 103
        $context["content_classes"] = [0 => (((twig_get_attribute($this->env, $this->source,         // line 104
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 104) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 104))) ? ("col-sm-6") : ("")), 1 => (((twig_get_attribute($this->env, $this->source,         // line 105
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 105) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 105)))) ? ("col-sm-9") : ("")), 2 => (((twig_get_attribute($this->env, $this->source,         // line 106
($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 106) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 106)))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty(twig_get_attribute($this->env, $this->source,         // line 107
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 107)) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 107)))) ? ("col-sm-12") : (""))];
        // line 110
        echo "        ";
        // line 111
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 111)) {
            // line 112
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 115
            echo "        ";
        }
        // line 116
        echo "
        ";
        // line 118
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 118)) {
            // line 119
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 122
            echo "        ";
        }
        // line 123
        echo "
    <section id=\"hero\" class=\"about_us\">
      <div class=\"hero-container about_us\" data-aos=\"fade-down\">
      <div class=\"row\">
        <div class=\"col-lg-12\">          
            <h1>";
        // line 128
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "label", [], "any", false, false, true, 128), 128, $this->source), "html", null, true);
        echo "</h1>
          </div> 
      </div>  
      </div>   
    </section>

        ";
        // line 135
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 139
        echo "
      ";
        // line 141
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 141)) {
            // line 142
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 147
            echo "      ";
        }
    }

    // line 85
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 86
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 87
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 87), 87, $this->source), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 94
    public function block_sidebar_first($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 95
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 96
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 96), 96, $this->source), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 112
    public function block_highlighted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 113
        echo "            ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 113), 113, $this->source), "html", null, true);
        echo "
          ";
    }

    // line 119
    public function block_help($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 120
        echo "            ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 120), 120, $this->source), "html", null, true);
        echo "
          ";
    }

    // line 135
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 136
        echo "          <a id=\"main-content\"></a>
          ";
        // line 137
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 137), 137, $this->source), "html", null, true);
        echo "
        ";
    }

    // line 142
    public function block_sidebar_second($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 143
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 144
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 144), 144, $this->source), "html", null, true);
        echo "
          </aside>
        ";
    }

    public function getTemplateName()
    {
        return "themes/custom/machtig/templates/system/page--machtig-page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 144,  340 => 143,  336 => 142,  330 => 137,  327 => 136,  323 => 135,  316 => 120,  312 => 119,  305 => 113,  301 => 112,  294 => 96,  291 => 95,  287 => 94,  280 => 87,  277 => 86,  273 => 85,  268 => 147,  265 => 142,  262 => 141,  259 => 139,  256 => 135,  247 => 128,  240 => 123,  237 => 122,  234 => 119,  231 => 118,  228 => 116,  225 => 115,  222 => 112,  219 => 111,  217 => 110,  215 => 107,  214 => 106,  213 => 105,  212 => 104,  211 => 103,  209 => 102,  206 => 100,  203 => 99,  200 => 94,  197 => 93,  194 => 91,  191 => 90,  188 => 85,  185 => 84,  182 => 82,  178 => 81,  173 => 76,  169 => 74,  167 => 73,  164 => 72,  158 => 70,  156 => 69,  151 => 68,  145 => 66,  143 => 65,  138 => 64,  136 => 60,  134 => 59,  130 => 58,  125 => 183,  119 => 181,  117 => 180,  109 => 174,  103 => 172,  101 => 171,  96 => 168,  90 => 166,  88 => 165,  83 => 162,  77 => 160,  75 => 159,  63 => 149,  61 => 81,  58 => 79,  54 => 58,  52 => 57,  50 => 55,  47 => 54,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title: The page title, for use in the actual content.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - messages: Status and error messages. Should be displayed prominently.
 * - tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.navigation: Items for the navigation region.
 * - page.navigation_collapsible: Items for the navigation (collapsible) region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 *
 * @ingroup templates
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}

{% set container = theme.settings.fluid_container ? 'container d-flex align-items-center justify-content-between' : 'container d-flex align-items-center justify-content-between' %}
{# Navbar #}
{% if page.navigation or page.navigation_collapsible %}
  {% block navbar %}
    {%
      set navbar_classes = [
        'fixed-top',
      ]
    %}
    <header{{ navbar_attributes.addClass(navbar_classes) }} id=\"header\" role=\"banner\">
      {% if not navbar_attributes.hasClass(container) %}
        <div class=\"{{ container }}\">
      {% endif %}
      <h1 class=\"logo\"><img src=\"{{logopath}}\"></h1>
      {% if page.navigation_collapsible %}
        {{ page.navigation_collapsible }}
      {% endif %}

      {% if not navbar_attributes.hasClass(container) %}
        </div>
      {% endif %}
    </header>
  {% endblock %}
{% endif %}

{# Main #}
{% block main %}
  <main id=\"main\">
      {# Header #}
      {% if page.header %}
        {% block header %}
          <div class=\"col-sm-12\" role=\"heading\">
            {{ page.header }}
          </div>
        {% endblock %}
      {% endif %}

      {# Sidebar First #}
      {% if page.sidebar_first %}
        {% block sidebar_first %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_first }}
          </aside>
        {% endblock %}
      {% endif %}

      {# Content #}
      {%
        set content_classes = [
          page.sidebar_first and page.sidebar_second ? 'col-sm-6',
          page.sidebar_first and page.sidebar_second is empty ? 'col-sm-9',
          page.sidebar_second and page.sidebar_first is empty ? 'col-sm-9',
          page.sidebar_first is empty and page.sidebar_second is empty ? 'col-sm-12'
        ]
      %}
        {# Highlighted #}
        {% if page.highlighted %}
          {% block highlighted %}
            {{ page.highlighted }}
          {% endblock %}
        {% endif %}

        {# Help #}
        {% if page.help %}
          {% block help %}
            {{ page.help }}
          {% endblock %}
        {% endif %}

    <section id=\"hero\" class=\"about_us\">
      <div class=\"hero-container about_us\" data-aos=\"fade-down\">
      <div class=\"row\">
        <div class=\"col-lg-12\">          
            <h1>{{ node.label }}</h1>
          </div> 
      </div>  
      </div>   
    </section>

        {# Content #}
        {% block content %}
          <a id=\"main-content\"></a>
          {{ page.content }}
        {% endblock %}

      {# Sidebar Second #}
      {% if page.sidebar_second %}
        {% block sidebar_second %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_second }}
          </aside>
        {% endblock %}
      {% endif %}
{% endblock %}


<a href=\"#\" class=\"back-to-top d-flex align-items-center justify-content-center\"><i class=\"bi bi-arrow-up-short\"></i></a>


<footer id=\"footer\">
  <div class=\"footer-top\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-contact\" data-aos=\"fade-down\">
        {% if page.footer_first %}
          {{ page.footer_first }}
          {% endif %}
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Useful Links</h4>
          {% if page.footer_second %}
          {{ page.footer_second }}
          {% endif %}
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Our Services</h4>
          {% if page.footer_third %}
          {{ page.footer_third }}
          {% endif %}
        </div>
      </div>
    </div>
  </div>

  <div class=\"container d-md-flex py-4\">
    {% if page.footer %}
      {{ page.footer }}
    {% endif %}
    </div>
</footer>", "themes/custom/machtig/templates/system/page--machtig-page.html.twig", "C:\\xampp\\htdocs\\drupalsite1\\web\\themes\\custom\\machtig\\templates\\system\\page--machtig-page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 55, "if" => 57, "block" => 58);
        static $filters = array("escape" => 160);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
