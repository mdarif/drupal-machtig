<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/machtig/templates/menu/menu--main.html.twig */
class __TwigTemplate_57588c7732eb667c98b54948adb082989d086e9d8489d64739206b4252d71ad2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "
";
        // line 49
        $macros["menus"] = $this->macros["menus"] = $this;
        // line 50
        echo "
";
        // line 58
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_call_macro($macros["menus"], "macro_menu_links", [($context["items"] ?? null), ($context["attributes"] ?? null), 0, ($context["menu_name"] ?? null)], 58, $context, $this->getSourceContext()));
        echo " ";
        echo " 

";
    }

    // line 60
    public function macro_menu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, $__menu_name__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "menu_name" => $__menu_name__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            echo " ";
            echo " 
  ";
            // line 61
            $macros["menus"] = $this;
            echo " 
  ";
            // line 62
            echo " 
  ";
            // line 64
            $context["menu_classes"] = [];
            // line 67
            echo " 
  ";
            // line 68
            echo " 
  ";
            // line 70
            $context["submenu_classes"] = [];
            // line 74
            echo "  ";
            if (($context["items"] ?? null)) {
                echo " 
    ";
                // line 75
                if ((($context["menu_level"] ?? null) == 0)) {
                    echo " 
      <ul";
                    // line 76
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["menu_classes"] ?? null)], "method", false, false, true, 76), 76, $this->source), "html", null, true);
                    echo "> ";
                    echo " 
    ";
                } else {
                    // line 77
                    echo " 
      <ul";
                    // line 78
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "removeClass", [0 => ($context["menu_classes"] ?? null)], "method", false, false, true, 78), "addClass", [0 => ($context["submenu_classes"] ?? null)], "method", false, false, true, 78), 78, $this->source), "html", null, true);
                    echo "> ";
                    echo " 
    ";
                }
                // line 80
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    echo " 
      ";
                    // line 82
                    echo "       
       ";
                    // line 84
                    $context["item_classes"] = [0 => ((twig_get_attribute($this->env, $this->source,                     // line 85
$context["item"], "in_active_trail", [], "any", false, false, true, 85)) ? ("active") : ("")), 1 => ((twig_get_attribute($this->env, $this->source,                     // line 86
$context["item"], "is_active_trail", [], "any", false, false, true, 86)) ? ("active") : (""))];
                    // line 89
                    echo "
      ";
                    // line 91
                    $context["link_classes"] = [0 => "nav-link", 1 => "scrollto", 2 => ((twig_get_attribute($this->env, $this->source,                     // line 94
$context["item"], "is_expanded", [], "any", false, false, true, 94)) ? ((("c-menu-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["menu_name"] ?? null), 94, $this->source))) . "__item--expanded")) : ("")), 3 => ((twig_get_attribute($this->env, $this->source,                     // line 95
$context["item"], "is_collapsed", [], "any", false, false, true, 95)) ? ((("c-menu-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["menu_name"] ?? null), 95, $this->source))) . "__item--collapsed")) : ("")), 4 => ((twig_get_attribute($this->env, $this->source,                     // line 96
$context["item"], "in_active_trail", [], "any", false, false, true, 96)) ? ("menu-item--active-trail") : ("")), 5 => ((twig_get_attribute($this->env, $this->source,                     // line 97
$context["item"], "in_active_trail", [], "any", false, false, true, 97)) ? ("active") : (""))];
                    // line 100
                    echo "      <li ";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, true, 100), "addClass", [0 => ($context["item_classes"] ?? null)], "method", false, false, true, 100), 100, $this->source), "html", null, true);
                    echo ">
        ";
                    // line 102
                    echo "        ";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getLink($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,                     // line 104
$context["item"], "title", [], "any", false, false, true, 104), 104, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,                     // line 105
$context["item"], "url", [], "any", false, false, true, 105), 105, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                     // line 106
$context["item"], "attributes", [], "any", false, false, true, 106), "addClass", [0 => ($context["link_classes"] ?? null)], "method", false, false, true, 106), 106, $this->source)), "html", null, true);
                    // line 108
                    echo "
        ";
                    // line 109
                    if (twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 109)) {
                        // line 110
                        echo "          ";
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_call_macro($macros["menus"], "macro_menu_links", [twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 110), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1), ($context["menu_name"] ?? null)], 110, $context, $this->getSourceContext()));
                        echo " ";
                        // line 111
                        echo "        ";
                    }
                    // line 112
                    echo "      </li>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 114
                echo "    </ul>
  ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/machtig/templates/menu/menu--main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 114,  161 => 112,  158 => 111,  154 => 110,  152 => 109,  149 => 108,  147 => 106,  146 => 105,  145 => 104,  143 => 102,  138 => 100,  136 => 97,  135 => 96,  134 => 95,  133 => 94,  132 => 91,  129 => 89,  127 => 86,  126 => 85,  125 => 84,  122 => 82,  115 => 80,  109 => 78,  106 => 77,  100 => 76,  96 => 75,  91 => 74,  89 => 70,  86 => 68,  83 => 67,  81 => 64,  78 => 62,  74 => 61,  55 => 60,  47 => 58,  44 => 50,  42 => 49,  39 => 48,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation to display a menu.
 *
 * Available variables:
 * - classes: A list of classes to apply to the top level <ul> element.
 * - dropdown_classes: A list of classes to apply to the dropdown <ul> element.
 * - menu_name: The machine name of the menu.
 * - items: A nested list of menu items. Each menu item contains:
 *   - attributes: HTML attributes for the menu item.
 *   - below: The menu item child items.
 *   - title: The menu link title.
 *   - url: The menu link url, instance of \\Drupal\\Core\\Url
 *   - localized_options: Menu link localized options.
 *
 * @ingroup templates
 */
#}
{#
/**
 * @file
 * Default theme implementation to display a menu.
 *
 * Available variables:
 * - menu_name: The machine name of the menu.
 * - items: A nested list of menu items. Each menu item contains:
 *   - attributes: HTML attributes for the menu item.
 *   - below: The menu item child items.
 *   - title: The menu link title.
 *   - url: The menu link url, instance of \\Drupal\\Core\\Url
 *   - localized_options: Menu link localized options.
 *   - is_expanded: TRUE if the link has visible children within the current
 *     menu tree.
 *   - is_collapsed: TRUE if the link has children within the current menu tree
 *     that are not currently visible.
 *   - in_active_trail: TRUE if the link is in the active trail.
 *
 * @ingroup themeable
 */
#}
{#
/**
 * @file
 * Theme override to display a menu.
 */
#}

{% import _self as menus %}

{# 
  We call a macro which calls itself to render the full tree. 
  @see http://twig.sensiolabs.org/doc/tags/macro.html 
  
  1. We use menu_name (see above) to create a CSS class name from it. 
  See https://www.drupal.org/node/2649076 
#}
{{ menus.menu_links(items, attributes, 0, menu_name) }} {# 1. #} 

{% macro menu_links(items, attributes, menu_level, menu_name) %} {# 1. #} 
  {% import _self as menus %} 
  {# 1. #} 
  {% 
    set menu_classes = [ 
      
    ] 
  %} 
  {# 1. #} 
  {%
    set submenu_classes = [ 
       
    ] 
  %}
  {% if items %} 
    {% if menu_level == 0 %} 
      <ul{{ attributes.addClass(menu_classes) }}> {# 1. #} 
    {% else %} 
      <ul{{ attributes.removeClass(menu_classes).addClass(submenu_classes) }}> {# 1. #} 
    {% endif %}
    {% for item in items %} 
      {# 1. #}
       
       {% 
        set item_classes = [
          item.in_active_trail ? 'active',
          item.is_active_trail ? 'active',
        ]
      %}

      {% 
        set link_classes = [
          'nav-link',
          'scrollto',
          item.is_expanded ? 'c-menu-' ~ menu_name|clean_class ~ '__item--expanded', 
          item.is_collapsed ? 'c-menu-' ~ menu_name|clean_class ~ '__item--collapsed', 
          item.in_active_trail ? 'menu-item--active-trail',
          item.in_active_trail ? 'active',
        ]
      %}
      <li {{ item.attributes.addClass(item_classes) }}>
        {# 1. #}
        {{ 
          link( 
            item.title, 
            item.url, 
            item.attributes.addClass(link_classes) 
          ) 
        }}
        {% if item.below %}
          {{ menus.menu_links(item.below, attributes, menu_level + 1, menu_name) }} {# 1. #}
        {% endif %}
      </li>
    {% endfor %}
    </ul>
  {% endif %}
{% endmacro %}
", "themes/custom/machtig/templates/menu/menu--main.html.twig", "C:\\xampp\\htdocs\\drupalsite1\\web\\themes\\custom\\machtig\\templates\\menu\\menu--main.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("import" => 49, "macro" => 60, "set" => 64, "if" => 74, "for" => 80);
        static $filters = array("escape" => 76, "clean_class" => 94);
        static $functions = array("link" => 103);

        try {
            $this->sandbox->checkSecurity(
                ['import', 'macro', 'set', 'if', 'for'],
                ['escape', 'clean_class'],
                ['link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
