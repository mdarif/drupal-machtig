<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/machtig/templates/system/page.html.twig */
class __TwigTemplate_75387dd3b366611cb44f47ee790f89e1cde9d170d73b3b21800fb1b2bb0d56af extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "
";
        // line 55
        $context["container"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["theme"] ?? null), "settings", [], "any", false, false, true, 55), "fluid_container", [], "any", false, false, true, 55)) ? ("container d-flex align-items-center justify-content-between") : ("container d-flex align-items-center justify-content-between"));
        // line 57
        if ((twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation", [], "any", false, false, true, 57) || twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 57))) {
            // line 58
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 80
        echo "
";
        // line 82
        $this->displayBlock('main', $context, $blocks);
        // line 140
        echo "

<a href=\"#\" class=\"back-to-top d-flex align-items-center justify-content-center\"><i class=\"bi bi-arrow-up-short\"></i></a>


<footer id=\"footer\">
  <div class=\"footer-top\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-contact\" data-aos=\"fade-down\">
        ";
        // line 150
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_first", [], "any", false, false, true, 150)) {
            // line 151
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_first", [], "any", false, false, true, 151), 151, $this->source), "html", null, true);
            echo "
          ";
        }
        // line 153
        echo "        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Useful Links</h4>
          ";
        // line 156
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_second", [], "any", false, false, true, 156)) {
            // line 157
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_second", [], "any", false, false, true, 157), 157, $this->source), "html", null, true);
            echo "
          ";
        }
        // line 159
        echo "        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Our Services</h4>
          ";
        // line 162
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_third", [], "any", false, false, true, 162)) {
            // line 163
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_third", [], "any", false, false, true, 163), 163, $this->source), "html", null, true);
            echo "
          ";
        }
        // line 165
        echo "        </div>
      </div>
    </div>
  </div>

  <div class=\"container d-md-flex py-4\">
    ";
        // line 171
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 171)) {
            // line 172
            echo "      ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 172), 172, $this->source), "html", null, true);
            echo "
    ";
        }
        // line 174
        echo "    </div>
</footer>";
    }

    // line 58
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    ";
        // line 60
        $context["navbar_classes"] = [0 => "fixed-top"];
        // line 64
        echo "    <header";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "addClass", [0 => ($context["navbar_classes"] ?? null)], "method", false, false, true, 64), 64, $this->source), "html", null, true);
        echo " id=\"header\" role=\"banner\">
      ";
        // line 65
        if ( !twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method", false, false, true, 65)) {
            // line 66
            echo "        <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null), 66, $this->source), "html", null, true);
            echo "\">
      ";
        }
        // line 68
        echo "      
      <a href=\"";
        // line 69
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
        echo "\"><h1 class=\"logo\"><img src=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logopath"] ?? null), 69, $this->source), "html", null, true);
        echo "\"></h1></a>
      ";
        // line 70
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 70)) {
            // line 71
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation_collapsible", [], "any", false, false, true, 71), 71, $this->source), "html", null, true);
            echo "
      ";
        }
        // line 73
        echo "
      ";
        // line 74
        if ( !twig_get_attribute($this->env, $this->source, ($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method", false, false, true, 74)) {
            // line 75
            echo "        </div>
      ";
        }
        // line 77
        echo "    </header>
  ";
    }

    // line 82
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 83
        echo "  <main id=\"main\">
      ";
        // line 85
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 85)) {
            // line 86
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 91
            echo "      ";
        }
        // line 92
        echo "
      ";
        // line 94
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 94)) {
            // line 95
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 100
            echo "      ";
        }
        // line 101
        echo "
      ";
        // line 103
        echo "      ";
        // line 104
        $context["content_classes"] = [0 => (((twig_get_attribute($this->env, $this->source,         // line 105
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 105) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 105))) ? ("col-sm-6") : ("")), 1 => (((twig_get_attribute($this->env, $this->source,         // line 106
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 106) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 106)))) ? ("col-sm-9") : ("")), 2 => (((twig_get_attribute($this->env, $this->source,         // line 107
($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 107) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 107)))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty(twig_get_attribute($this->env, $this->source,         // line 108
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 108)) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 108)))) ? ("col-sm-12") : (""))];
        // line 111
        echo "        ";
        // line 112
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 112)) {
            // line 113
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 116
            echo "        ";
        }
        // line 117
        echo "
        ";
        // line 119
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 119)) {
            // line 120
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 123
            echo "        ";
        }
        // line 124
        echo "
        ";
        // line 126
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 130
        echo "
      ";
        // line 132
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 132)) {
            // line 133
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 138
            echo "      ";
        }
    }

    // line 86
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 87
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 88
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 88), 88, $this->source), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 95
    public function block_sidebar_first($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 97
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 97), 97, $this->source), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 113
    public function block_highlighted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 114
        echo "            ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 114), 114, $this->source), "html", null, true);
        echo "
          ";
    }

    // line 120
    public function block_help($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 121
        echo "            ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 121), 121, $this->source), "html", null, true);
        echo "
          ";
    }

    // line 126
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 127
        echo "          <a id=\"main-content\"></a>
          ";
        // line 128
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 128), 128, $this->source), "html", null, true);
        echo "
        ";
    }

    // line 133
    public function block_sidebar_second($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 134
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 135
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 135), 135, $this->source), "html", null, true);
        echo "
          </aside>
        ";
    }

    public function getTemplateName()
    {
        return "themes/custom/machtig/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 135,  331 => 134,  327 => 133,  321 => 128,  318 => 127,  314 => 126,  307 => 121,  303 => 120,  296 => 114,  292 => 113,  285 => 97,  282 => 96,  278 => 95,  271 => 88,  268 => 87,  264 => 86,  259 => 138,  256 => 133,  253 => 132,  250 => 130,  247 => 126,  244 => 124,  241 => 123,  238 => 120,  235 => 119,  232 => 117,  229 => 116,  226 => 113,  223 => 112,  221 => 111,  219 => 108,  218 => 107,  217 => 106,  216 => 105,  215 => 104,  213 => 103,  210 => 101,  207 => 100,  204 => 95,  201 => 94,  198 => 92,  195 => 91,  192 => 86,  189 => 85,  186 => 83,  182 => 82,  177 => 77,  173 => 75,  171 => 74,  168 => 73,  162 => 71,  160 => 70,  154 => 69,  151 => 68,  145 => 66,  143 => 65,  138 => 64,  136 => 60,  134 => 59,  130 => 58,  125 => 174,  119 => 172,  117 => 171,  109 => 165,  103 => 163,  101 => 162,  96 => 159,  90 => 157,  88 => 156,  83 => 153,  77 => 151,  75 => 150,  63 => 140,  61 => 82,  58 => 80,  54 => 58,  52 => 57,  50 => 55,  47 => 54,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title: The page title, for use in the actual content.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - messages: Status and error messages. Should be displayed prominently.
 * - tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.navigation: Items for the navigation region.
 * - page.navigation_collapsible: Items for the navigation (collapsible) region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 *
 * @ingroup templates
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}

{% set container = theme.settings.fluid_container ? 'container d-flex align-items-center justify-content-between' : 'container d-flex align-items-center justify-content-between' %}
{# Navbar #}
{% if page.navigation or page.navigation_collapsible %}
  {% block navbar %}
    {%
      set navbar_classes = [
        'fixed-top',
      ]
    %}
    <header{{ navbar_attributes.addClass(navbar_classes) }} id=\"header\" role=\"banner\">
      {% if not navbar_attributes.hasClass(container) %}
        <div class=\"{{ container }}\">
      {% endif %}
      
      <a href=\"{{ url('<front>') }}\"><h1 class=\"logo\"><img src=\"{{logopath}}\"></h1></a>
      {% if page.navigation_collapsible %}
        {{ page.navigation_collapsible }}
      {% endif %}

      {% if not navbar_attributes.hasClass(container) %}
        </div>
      {% endif %}
    </header>
  {% endblock %}
{% endif %}

{# Main #}
{% block main %}
  <main id=\"main\">
      {# Header #}
      {% if page.header %}
        {% block header %}
          <div class=\"col-sm-12\" role=\"heading\">
            {{ page.header }}
          </div>
        {% endblock %}
      {% endif %}

      {# Sidebar First #}
      {% if page.sidebar_first %}
        {% block sidebar_first %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_first }}
          </aside>
        {% endblock %}
      {% endif %}

      {# Content #}
      {%
        set content_classes = [
          page.sidebar_first and page.sidebar_second ? 'col-sm-6',
          page.sidebar_first and page.sidebar_second is empty ? 'col-sm-9',
          page.sidebar_second and page.sidebar_first is empty ? 'col-sm-9',
          page.sidebar_first is empty and page.sidebar_second is empty ? 'col-sm-12'
        ]
      %}
        {# Highlighted #}
        {% if page.highlighted %}
          {% block highlighted %}
            {{ page.highlighted }}
          {% endblock %}
        {% endif %}

        {# Help #}
        {% if page.help %}
          {% block help %}
            {{ page.help }}
          {% endblock %}
        {% endif %}

        {# Content #}
        {% block content %}
          <a id=\"main-content\"></a>
          {{ page.content }}
        {% endblock %}

      {# Sidebar Second #}
      {% if page.sidebar_second %}
        {% block sidebar_second %}
          <aside class=\"col-sm-3\" role=\"complementary\">
            {{ page.sidebar_second }}
          </aside>
        {% endblock %}
      {% endif %}
{% endblock %}


<a href=\"#\" class=\"back-to-top d-flex align-items-center justify-content-center\"><i class=\"bi bi-arrow-up-short\"></i></a>


<footer id=\"footer\">
  <div class=\"footer-top\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-contact\" data-aos=\"fade-down\">
        {% if page.footer_first %}
          {{ page.footer_first }}
          {% endif %}
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Useful Links</h4>
          {% if page.footer_second %}
          {{ page.footer_second }}
          {% endif %}
        </div>
        <div class=\"col-lg-4 col-md-4 col-sm-12 footer-links\" data-aos=\"fade-down\">
          <h4>Our Services</h4>
          {% if page.footer_third %}
          {{ page.footer_third }}
          {% endif %}
        </div>
      </div>
    </div>
  </div>

  <div class=\"container d-md-flex py-4\">
    {% if page.footer %}
      {{ page.footer }}
    {% endif %}
    </div>
</footer>", "themes/custom/machtig/templates/system/page.html.twig", "C:\\xampp\\htdocs\\drupalsite1\\web\\themes\\custom\\machtig\\templates\\system\\page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 55, "if" => 57, "block" => 58);
        static $filters = array("escape" => 151);
        static $functions = array("url" => 69);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
